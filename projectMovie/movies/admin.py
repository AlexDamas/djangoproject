from django.contrib import admin
from movies.models import Serie, Film, Realisateur, Image

# Register your models here.

admin.site.register(Serie)
admin.site.register(Film)
admin.site.register(Realisateur)
admin.site.register(Image)