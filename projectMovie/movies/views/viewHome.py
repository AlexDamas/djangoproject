from django.shortcuts import render
from django.http import HttpResponse
from movies.models import Image
from movies.forms import ImageForm

# Create your views here.

def home(request) :
    return render(request, template_name='home.html')


def connexion(request) :
    return render(request, template_name='login.html')


def showimage(request):
    image= Image.objects.all()
    form= ImageForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()
    context= {'image': image,
              'form': form
              }
    return render(request, template_name='images.html', context=context)
