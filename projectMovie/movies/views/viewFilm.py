from django.shortcuts import render
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from movies.models import Film, Realisateur

# Create your views here.

class FilmList(ListView):
    model = Film

class FilmDetailView(DetailView):
    model = Film

class FilmCreate(CreateView):
    model = Film
    fields = ['nom', 'description', 'sortie', 'realisateurs', 'img']
    success_url = reverse_lazy('movies:film_list')

class FilmUpdate(UpdateView):
    model = Film
    fields = ['nom', 'description', 'sortie', 'realisateurs', 'img']
    template_name_suffix = '_update_form'
    success_url = reverse_lazy('movies:film_list')

class FilmDelete(DeleteView):
    model = Film
    success_url = reverse_lazy('movies:film_list')

