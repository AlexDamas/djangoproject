from django.urls import path
from .views import SerieListView, SerieDetailView, SerieUpdateView, SerieCreateView, SerieDeleteView
from .views import FilmList, FilmDetailView, FilmCreate, FilmUpdate, FilmDelete, home, connexion, showimage
from .views import RealisateurListView, RealisateurDetailView, RealisateurUpdateView, RealisateurCreateView,RealisateurDeleteView

app_name = 'movies' # Encapsule les urls de ce module dans le namespace musique

urlpatterns = [
    path('series', SerieListView.as_view(), name='serie_list'),
    path('series/', SerieListView.as_view(), name='serie_list'),
    path('series/<int:pk>', SerieDetailView.as_view(), name='serie_detail'),
    path('series/edit/<int:pk>', SerieUpdateView.as_view(), name='serie_edit'),
    path('series/create', SerieCreateView.as_view(), name='serie_create'),
    path('series/delete/<int:pk>', SerieDeleteView.as_view(), name='serie_delete'),
    path('home', home, name='home'),
    path('home/', home, name='home'),
    path('films', FilmList.as_view(), name='film_list'),
    path('films/', FilmList.as_view(), name='film_list'),
    path('films/<int:pk>', FilmDetailView.as_view(), name='film_details'),
    path('films/add', FilmCreate.as_view(), name='nouveau_film'),
    path('films/update/<int:pk>', FilmUpdate.as_view(), name='modif_film'),
    path('films/delete/<int:pk>', FilmDelete.as_view(), name='sup_film'),
    path('realisateur', RealisateurListView.as_view(), name='realisateur_list'),
    path('realisateur/', RealisateurListView.as_view(), name='realisateur_list'),
    path('realisateur/<int:pk>', RealisateurDetailView.as_view(), name='realisateur_detail'),
    path('realisateur/edit/<int:pk>', RealisateurUpdateView.as_view(), name='realisateur_edit'),
    path('realisateur/create', RealisateurCreateView.as_view(), name='realisateur_create'),
    path('realisateur/delete/<int:pk>', RealisateurDeleteView.as_view(), name='realisateur_delete'),
    path('login/', connexion, name='connexion'),
    path('img/', showimage, name='image')
    # path('series', test, name='home-serie')
]

