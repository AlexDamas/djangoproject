import scrapy
import re
from urllib.parse import urljoin
from scrap.items import RealisateurItem,SerieItem, SerieRealItem

class SerieSpider(scrapy.Spider):
    name = "serie"
    start_urls = [
         "https://www.senscritique.com/liste/Mes_series_et_feuilletons/154972/page-1",
         ## Pour scraper plusieurs pages, changer la fin de l'url ou decommenter les lignes
         ## du dessous
         # "https://www.senscritique.com/liste/Mes_series_et_feuilletons/154972/page-2",
         # "https://www.senscritique.com/liste/Mes_series_et_feuilletons/154972/page-3",
         # "https://www.senscritique.com/liste/Mes_series_et_feuilletons/154972/page-4",
         # "https://www.senscritique.com/liste/Mes_series_et_feuilletons/154972/page-5",
    ]

    def parse(self,response):
        for sel in response.xpath('//div//@data-sc-link'):
            if "/serie" in sel.get():
                base_url = "https://www.senscritique.com/"
                final_url = urljoin(base_url, sel.get())
                yield scrapy.Request(final_url, callback=self.parse_serie)

    def parse_serie(self, response):
        for data in response.xpath('//div[@class="wrap__content"]'):
            item = {}
            serie = SerieRealItem()
            serie['nom'] = "".join(data.xpath('//section/div/div/div/div[1]/h1//text()').extract()).strip()
            serie['description'] = "".join(data.xpath('//div[3]/div/section[1]/p/text()').extract()).strip()
            saison = data.xpath('//div[2]/div[3]/div/section[1]/ul/li[3]//text()').get()
            if "saison" not in saison:
                serie['nb_saison'] = "1"
            serie['nb_saison'] = "".join(re.findall('\d+', saison))
            serie['link_img'] = "".join(data.xpath('//div[2]/section/div/div/figure/a/@href').extract()).strip()
            serie['sortie'] = "".join(data.xpath('//li[@class="pvi-productDetails-item nowrap"]//time/@datetime').extract())
            item['serie'] = serie
    
            for sel in response.xpath('//div[2]/div[3]/div/section[1]/ul/li[1]/span[1]//a//@href'):
                base_url = "https://www.senscritique.com/"
                final_url = urljoin(base_url, sel.get())
                request = scrapy.Request(final_url, callback=self.parse_real)
                request.meta['item'] = item
                yield request

    def parse_real(self, response):
        for data in response.xpath('/html/body/div[1]/div[2]/section'):
            item  = response.meta['item']
            real = RealisateurItem()
            real['nom'] = "".join(data.xpath('//div/div//h1//text()').extract()).strip()
            real['description'] = "".join(data.xpath('//div/div/p//text()').extract()).strip()
            real['link_img'] = "".join(data.xpath('//div/img/@src').extract()).strip()
            item['realisateurs'] = real
            yield item
