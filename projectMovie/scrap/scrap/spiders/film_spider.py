import scrapy
import re
from urllib.parse import urljoin
from scrap.items import FilmItem, RealisateurItem

class FilmSpider(scrapy.Spider):
    name = "film"
    start_urls = [
         "https://www.senscritique.com/films/toujours-a-l-affiche",
    ]


    def parse(self,response):
        for sel in response.xpath('//div//@data-sc-link'):
            if "/film" in sel.get():
                base_url = "https://www.senscritique.com/"
                final_url = urljoin(base_url, sel.get())
                yield scrapy.Request(final_url, callback=self.parse_film)

    def parse_film(self, response):
        for data in response.xpath('//div[@class="wrap"]'):
            item = FilmItem()
            itemr = RealisateurItem()
            item['nom'] = "".join(data.xpath('//section/div/div/div/div[1]/h1//text()').extract())
            item['sortie'] = "".join(data.xpath('//li[@class="pvi-productDetails-item nowrap"]//time/@datetime').extract())
            item['description'] = "".join(data.xpath('//div[3]/div/section[1]/p/text()').extract())
            item['link_img'] = "".join(data.xpath('//div[@class="wrap__content"]/section/div/div/figure//a/@href').extract())
            real = "".join(data.xpath('//div/div[@class="d-grid-extend"]/div/section[@class="pvi-productDetails"]/ul/li/span/a/span//text()').extract())
            nom_real = real.split()[1]
            prenom_real = real.split()[0]
            print(nom_real)
            print(prenom_real)
            if itemr.django_model.getRealbyNom(nom = nom_real, prenom = prenom_real):
                print("existe")
            else : 
                print('existe pas')
            item.save()
