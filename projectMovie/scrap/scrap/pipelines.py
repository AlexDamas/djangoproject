# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from .items import SerieRealItem, RealisateurItem
from movies.models import Serie, Realisateur
 
class SerieRealPipeline(object):
    def process_item(self, item, spider):
        def check_real(nom):
            try:
                ins = Realisateur.objects.get(nom=nom)
                return ins
            except:
                return None

        def check_serie(nom):
            try:
                instance = Serie.objects.get(nom=nom)
                return instance
            except:
                return None

        if isinstance(item, dict):

            # Extract two ITEM
            serie = item['serie']
            realisateur = item['realisateurs']
            r_nom = realisateur['nom']
            s_nom = serie['nom']

            # On regarde si réalisateurs n'existe pas déja
            if check_real(r_nom):
                realisateur = check_real(r_nom)
            realisateur.save()
            # Meme chose pour serie
            if check_serie(s_nom):
                serie = check_serie(s_nom)
            serie.save()
            
            real = Realisateur.objects.get(nom=r_nom)
            series = Serie.objects.get(nom=s_nom)
            series.realisateurs.add(real)
            
            return item